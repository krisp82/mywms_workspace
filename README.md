# myWms workspace based on eclipse




> Pre-requisite
    1. WildFly 8.0
    2. PostgreSQL (already created role:jboss, password: jboss, created database to be 'los.reference')
    3. Eclipse
    4. clone https://gitlab.com/krisp82/mywms

# How to run in local?
1. Follow a video
2. If there is any red warning, check library path.
-  Right click on the project => Choose Build Path => Configure Build Path
- Tab 'libaries'
        If there is any red issue, it may about invalid path, set a new path based on this repo 'https://gitlab.com/krisp82/mywms'.
- Adding a new libary by Click at 'Add External Jars'.
- If there is a red issue about 'WildFly' library, adding the library by Click at 'Add Libary' => Server Runtime => Select 'WildFly'.
- Click 'Apply and Close'.
3. If there is still having a red warning, check at a 'server' tab by Click at 'Window' => 'Show View' => 'Other' => 'Server'.
- At 'Server' tab, Click 'Create a new server' => Select 'WildFly8.0' => Finish.
Done.